# gitlab-changelog-generation-pre-commit

<div style="display: flex; flex-direction: row; align-items: center;">

<a href="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit">
    <img alt="pipeline status" src="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit/badges/main/pipeline.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit/-/releases">
    <img alt="latest release" src="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit/-/badges/release.svg?order_by=release_at">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit">
    <img alt="latest release" src="https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit/badges/main/coverage.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://github.com/pypa/hatch">
    <img alt="hatch" src="https://img.shields.io/badge/%F0%9F%A5%9A-Hatch-4051b5.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://github.com/psf/black">
    <img alt="black" src="https://img.shields.io/badge/code%20style-black-000000.svg">
</a>
</div>
</div>

Pre-commit hook that updates CHANGELOG.md from the Gitlab API.

## Links

- Documentation : https://alric_sans.gitlab.io/gitlab-changelog-generation-pre-commit
- Issues : https://gitlab.com/alric_sans/gitlab-changelog-generation-pre-commit/-/issues
