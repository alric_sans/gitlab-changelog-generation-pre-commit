from gitlab_changelog_generation_pre_commit.generation import get_version, get_changelog,write_changelog
import shutil 
import pytest
import os

def test_get_version_ok(tmpdir):
    f1 = tmpdir.join("__init__.py")
    f1.write("__version__ = ''")
    subfolder = os.path.join(tmpdir, "dir2")
    if not os.path.exists(subfolder):
        os.makedirs(subfolder)
    f2 = os.path.join(subfolder, "__init__.py")
    with open(f2, 'w') as version_file:
        version_file.write("__version__ = '15.0.2'")

    assert get_version([str(f1), str(f2)], "__init__.py") == "15.0.2"

def test_get_version_no_version_in_file(tmpdir):
    f = tmpdir.join("__init__.py")
    f.write("__version__ = ''")

    assert get_version([str(f)], "__init__.py") == None

def test_get_version_no_file(tmpdir):
    f = tmpdir.join("__init__.py")
    f.write("__version__ = '15.0.2'")

    assert get_version([str(f)], "othername.py") == None

def test_get_changelog():
    url = "https://gitlab.com/api/v4/projects/48308032/repository/changelog"
    version = "1.0.0"
    assert f"## {version}" in get_changelog(url, version)

def test_get_changelog_bad_url():
    url = "https://gitlab.com/api/v4/projects/xxx/repository/changelog"
    version = "1.0.0"
    assert get_changelog(url, version) == None

def test_write_changelog_ok(tmpdir):
    changelog = "This is the content of the changelog"
    write_changelog(changelog, dir=tmpdir)
    assert os.path.isfile(os.path.join(tmpdir, "CHANGELOG.md"))
